import random
import math

#function
def lengthOfPoint(x1, y1, x2, y2):
    sum = math.sqrt(pow(x2-x1, 2)+pow(y2-y1, 2))
    return sum

#Code
kmeansData1 = ["Dadan", random.randint(18, 61), 3500000, random.randint(1, 8), "Yes"]
kmeansData2 = ["Budi", random.randint(18, 61), 5000000, random.randint(1, 8), "No"]
kmeansData3 = ["Dodi", random.randint(18, 61), 2000000, random.randint(1, 8), ""]
kmeansData4 = ["Amin", random.randint(18, 61), 1700000, random.randint(1, 8), ""]
kmeansData5 = ["Iwan", random.randint(18, 61), 4000000, random.randint(1, 8), ""]
kmeansData6 = ["Dodo", random.randint(18, 61), 5000000, random.randint(1, 8), ""]

centroid1 = [kmeansData1[1], kmeansData1[3]]
centroid2 = [kmeansData2[1], kmeansData2[3]]

kmeans_Clustering = [
    kmeansData1,
    kmeansData2,
    kmeansData3,
    kmeansData4,
    kmeansData5,
    kmeansData6
]

centroid = [
    centroid1,
    centroid2
]

print("Perubahan Centroid")
print("Centroid 1 - ("+str(centroid[0][0])+", "+str(centroid[0][1])+")")
print("Centroid 2 - ("+str(centroid[1][0])+", "+str(centroid[1][1])+")")
for i in range(2, 6):
    if lengthOfPoint(centroid[0][0], centroid[0][1], kmeans_Clustering[i][1], kmeans_Clustering[i][3]) > \
            lengthOfPoint(centroid[1][0], centroid[1][1], kmeans_Clustering[i][1], kmeans_Clustering[i][3]):
        kmeans_Clustering[i][4] = "No"
        centroid[1][0] = (centroid[1][0]+kmeans_Clustering[i][1])/2
        centroid[1][1] = (centroid[1][1]+kmeans_Clustering[i][3])/2
        print("Centroid 2 - (" + str(centroid[1][0]) + ", " + str(centroid[1][1]) + ")")
    else:
        kmeans_Clustering[i][4] = "Yes"
        centroid[0][0] = (centroid[0][0] + kmeans_Clustering[i][1])/2
        centroid[0][1] = (centroid[0][1] + kmeans_Clustering[i][3])/2
        print("Centroid 1 - (" + str(centroid[0][0]) + ", " + str(centroid[0][1]) + ")")

print("\nDataset Penerima Pinjaman Bank Syariah\nNama\tUmur\tPemasukan\tJumlah Rekening\tClass")
for data in kmeans_Clustering:
    print(data)
